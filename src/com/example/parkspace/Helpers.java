package com.example.parkspace;

import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

class Helpers {
	private static final int SECOND_IN_MILLIS = 10000;

	public static String getStringContent(String uri) throws Exception {
	    try {
	        HttpGet request = new HttpGet();
	        request.setURI(new URI(uri));
	        request.addHeader("Accept-Encoding", "gzip");
	
	        final HttpParams params = new BasicHttpParams();
	        HttpConnectionParams.setConnectionTimeout(params, 30 * SECOND_IN_MILLIS);
	        HttpConnectionParams.setSoTimeout(params, 30 * SECOND_IN_MILLIS);
	        HttpConnectionParams.setSocketBufferSize(params, 8192);
	
	        final DefaultHttpClient client = new DefaultHttpClient(params);
	        client.addResponseInterceptor(new HttpResponseInterceptor() {
	                public void process(HttpResponse response, HttpContext context) {
	                    final HttpEntity entity = response.getEntity();
	                    final Header encoding = entity.getContentEncoding();
	                    if (encoding != null) {
	                        for (HeaderElement element : encoding.getElements()) {
	                            if (element.getName().equalsIgnoreCase("gzip")) {
	                                response.setEntity(new InflatingEntity(response.getEntity()));
	                                break;
	                            }
	                        }
	                    }
	                }
	            });
	
	        return client.execute(request, new BasicResponseHandler());
	    } finally {
	        // any cleanup code...
	    }
	}
	
	private static class InflatingEntity extends HttpEntityWrapper {
        public InflatingEntity(HttpEntity wrapped) {
            super(wrapped);
        }

        @Override
        public InputStream getContent() throws IOException {
            return new GZIPInputStream(wrappedEntity.getContent());
        }

        @Override
        public long getContentLength() {
            return -1;
        }
    }
}