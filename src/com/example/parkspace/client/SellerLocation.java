package com.example.parkspace.client;

public class SellerLocation {
	private String sellerUid;
	private double latitude;
	private double longitude;

	public SellerLocation (String uid, double latitude, double longitude) {
		setSellerUid(uid);
		setLatitude(latitude);
		setLongitude(longitude);
	}

	public String getSellerUid() {
		return sellerUid;
	}
	public void setSellerUid(String sellerUid) {
		this.sellerUid = sellerUid;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return this.sellerUid + " at (" + this.latitude + ", " + this.longitude + ")";
	}

}
