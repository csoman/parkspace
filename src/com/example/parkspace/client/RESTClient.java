package com.example.parkspace.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import android.util.Log;

public class RESTClient {
	private static final String serviceURL = "http://ec2-204-236-160-57.us-west-1.compute.amazonaws.com:8080";	

	public static void registerUser (String uid) {
		try {
			URL url = new URL(serviceURL + "/register?uid=" + uid);
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());               
			}
			
			conn.disconnect();
		}catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static void registerParkingSlot (SellerLocation sellerLoc) {
		try {
			Log.e("ParkSpace", "Calling register slot");

			URL url = new URL(serviceURL + "/register_parking_slot?uid=" + sellerLoc.getSellerUid() + "&latitude=" + sellerLoc.getLatitude() + "&longitude=" + sellerLoc.getLongitude());
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			Log.e("ParkSpace", "Sending request");


			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());    
				Log.e("ParkSpace", "Error in registering location");

			}
			Log.e("ParkSpace", "Location registered successfully");
			conn.disconnect();

		}catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public static List<SellerLocation> getParkingLocations () {
		try {
			List <SellerLocation> locationsList = new ArrayList<SellerLocation>();
			URL url = new URL(serviceURL + "/get_parking_slots?uid=chinmay");
			//			URL url = new URL("http://localhost:8080/get_parking_slots?uid=chinmay");
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());  
				Log.d("RESTLOCS", "Illegal response " + conn.getResponseMessage());
			} else {
				BufferedReader msgReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = msgReader.readLine()) != null) {
					sb.append(line);
				}
				msgReader.close();
				System.out.println(sb);
				Log.d("RESTLOCS", "Response for request for seller locations: " + sb.toString());

				// Parse the JSON
				JSONArray locationArray = (JSONArray)JSONValue.parse(sb.toString());
				for (int i = 0; i < locationArray.size(); i++) {
					JSONObject sellerLoc = (JSONObject) locationArray.get(i);
					String uid = (String)sellerLoc.get("uid");
					double latitude = (Double) sellerLoc.get("latitude");
					double longitude = (Double) sellerLoc.get("longitude");
					SellerLocation locationOverlay = new SellerLocation(uid, latitude, longitude);
					locationsList.add(locationOverlay);
					System.out.println(locationOverlay);
				}
			}
			
			conn.disconnect();
			return locationsList;
		}catch (Exception e) {
			Log.d("RESTLOCS", "Error");
			e.printStackTrace();
		}
		
		return null;
	}

	
	public static String pollForBuyer (String uid) {
		try {
			URL url = new URL(serviceURL + "/poll_reserve_request?uid=" + uid);
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());               
			} else {
				BufferedReader msgReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = msgReader.readLine()) != null) {
					sb.append(line);
				}
				msgReader.close();
				
				if (!sb.toString().equals("***NULL***")) {
					return sb.toString();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void reservationRequest (String buyer, String seller) {
		try {
			Log.e("ParkSpace3", "Reservation book request");

			URL url = new URL(serviceURL + "/reserve_request?uid=" + buyer + "&seller=" + seller);
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage()); 
				Log.e("ParkSpace3", "Error in booking reservation");

			}
			Log.e("ParkSpace3", "Reservation booked successfully");

		}catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static String timedPollForBuyer(String uid, int timeInSeconds) {
		long currentTime = System.currentTimeMillis();
		long targetTime = currentTime + (timeInSeconds * 1000);
		String response = null;
		
		while (currentTime < targetTime) {
			response = pollForBuyer(uid);
			if (response == null) {
				System.out.println("Did not get anything ... sleeping for 500 milliseconds. ");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				currentTime = System.currentTimeMillis();
			} else {
				break;
			}
		}
		return response;
	}

}
