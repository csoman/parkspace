package com.example.parkspace;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.parkspace.client.RESTClient;
import com.example.parkspace.client.SellerLocation;
import com.google.android.maps.*;

class SometimesVisibleButtonItemizedOverlay extends ItemizedOverlay {
	private OverlayItem item = null;
	private Context context = null;
	
	public SometimesVisibleButtonItemizedOverlay(Drawable arg0, Context c) {
		super(boundCenterBottom(arg0));
		populate();
		context = c;
	}
	
	public OverlayItem getItem() {
		return item;
	}
	
	public void setItem(OverlayItem i) {
		item = i;
		populate();
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		Log.v("CURR LOC", item.getPoint().toString());
		return item;
	}
	
	@Override
	public int size() {
		if (item == null)
			return 0;
		else
			return 1;
	}
	
	public void draw(android.graphics.Canvas canvas,
			MapView mapView,
			boolean shadow) {

		super.draw(canvas, mapView, false);
	}
	
	private class LoadTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... locString) {
			SellerLocation sellerLoc = new SellerLocation(locString[0], Integer.parseInt(locString[1]), Integer.parseInt(locString[2]));     
			RESTClient.registerParkingSlot(sellerLoc);
			
			String potentialBuyer = RESTClient.timedPollForBuyer(locString[0], 300);
			if (potentialBuyer != null) {
				Log.e("ParkSpace3" , "Got a buyer !!!");
			}
			return null;
		}
	}

	public boolean onTouchEvent(MotionEvent event, MapView mapView) {
		if (item != null) {
			Log.v("CLICK", "NULL");
	    	if (event.getAction() == MotionEvent.ACTION_UP) {
	    		Log.v("CLICK", "UP x " + event.getX() + " y " + event.getY());
	    		Log.v("CLICK", "BOTTOM " + mapView.getBottom());
	    		if (event.getY() >= mapView.getBottom() - 116 - 150) {
	    			Log.v("CLICK", "Y");
	    			float x = event.getX();
	    			if (x >= mapView.getWidth() / 2 - 127 - 250 && x < mapView.getWidth() / 2 + 127 + 250) {
	    				//Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
	    				float y = event.getY();
	    				GeoPoint gp = mapView.getProjection().fromPixels((int)x, (int)y);
	    				//RESTClient.getParkingLocations();
	    				//RESTClient.registerParkingSlot(new SellerLocation("TEST", gp.getLatitudeE6(), gp.getLongitudeE6()));
	    				
	    				LoadTask les = new LoadTask();
	    		        les.execute(MainActivity.userId, Integer.toString(gp.getLatitudeE6()), Integer.toString(gp.getLongitudeE6()));
	    				
	    				Log.v("CLICK", "CLICK");
	    				
	    				
	    				
	    				return true;
	    			}
	    		}
	    	}
		}
    	return super.onTouchEvent(event, mapView);
	}
}