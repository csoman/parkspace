package com.example.parkspace;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;
	
import com.example.parkspace.client.RESTClient;
import com.example.parkspace.client.SellerLocation;
import com.google.android.maps.*;

class ExistingSellersItemizedOverlay extends ItemizedOverlay {
	private ArrayList<SellerLocation> locations;
	
	public ExistingSellersItemizedOverlay(Drawable arg0) {
		super(boundCenterBottom(arg0));
		locations = new ArrayList<SellerLocation>();
		populate();
	}
	
	public void addItem(SellerLocation i) {
		locations.add(i);
		populate();
	}
	
	public void clear() {
		this.locations.clear();
	}
	
	public void addItems(List<SellerLocation> items) {
		this.locations.addAll(items);
		populate();
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		SellerLocation loc = this.locations.get(i);
		GeoPoint gp = new GeoPoint((int)loc.getLatitude(), (int)loc.getLongitude());
		return new OverlayItem(gp, "Blank", "Blank");
	}
	
	@Override
	public int size() {
		return locations.size();
	}
	
	@Override
	protected boolean onTap (int i) {
		Log.e("ParkSpace2" , "Ontap event fired for index : " + i);
		ReserveTask les = new ReserveTask();
		Log.e("ParkSpace3", "Starting background task to book a slot");

        les.execute(MainActivity.userId, locations.get(i).getSellerUid());
		Log.e("ParkSpace3", " background task started");

		return true;
	}
	
	private class ReserveTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... locString) {
			Log.e("ParkSpace3", "Calling background task to book a slot");
			RESTClient.reservationRequest(locString[0], locString[1]);
			return null;
		}
		
	}
}

