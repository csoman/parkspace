package com.example.parkspace;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;

import com.google.android.maps.*;

class TouchMovingSingleItemizedOverlay extends ItemizedOverlay {
	private OverlayItem item;
	private SometimesVisibleButtonItemizedOverlay button = null;
	private boolean visibility = true;
	
	public TouchMovingSingleItemizedOverlay(Drawable arg0) {
		super(boundCenterBottom(arg0));
		item = new OverlayItem(new GeoPoint(MainActivity.DefaultLat, MainActivity.DefaultLon), "Not filled in", "Not filled in");
		populate();
	}
	
	public OverlayItem getItem() {
		return item;
	}
	
	public void makeVisible() {
		visibility = true;
		populate();
	}
	
	public void hide() {
		visibility = false;
		populate();
	}
	
	public void setItem(OverlayItem i) {
		item = i;
		populate();
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		Log.v("PINN LOC", item.getPoint().toString());
		return item;
	}
	
	@Override
	public int size() {
		if (visibility)
			return 1;
		else
			return 0;
	}
	
	void setTouchButton(SometimesVisibleButtonItemizedOverlay button) {
		this.button = button;
	}

    @Override
    public boolean onTouchEvent(MotionEvent event, MapView mapView) {
    	if (event.getAction() == MotionEvent.ACTION_UP) {
    		float x = event.getX();
    		float y = event.getY();
    		GeoPoint pt = mapView.getProjection().fromPixels((int)x, (int)y);
    		OverlayItem oi = new OverlayItem(pt, "Blank", "Blankblank");
    		setItem(oi);
    		GeoPoint buttonpt = mapView.getProjection().fromPixels(mapView.getWidth() / 2, mapView.getBottom() - 116);
    		OverlayItem buttonoi = new OverlayItem(buttonpt, "Blank", "Blankblank");
    		button.setItem(buttonoi);
    		Log.v("PARKSPACE", "Action up - adding item at: " + pt.toString());
    		//return true;
    	}
    	return super.onTouchEvent(event, mapView);
    }
	
}