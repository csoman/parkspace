package com.example.parkspace;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;

import com.google.android.maps.*;

class SingleItemizedOverlay extends ItemizedOverlay {
	private OverlayItem item;
	
	public SingleItemizedOverlay(Drawable arg0) {
		super(boundCenterBottom(arg0));
		item = new OverlayItem(new GeoPoint(MainActivity.DefaultLat, MainActivity.DefaultLon), "Not filled in", "Not filled in");
		populate();
	}
	
	public OverlayItem getItem() {
		return item;
	}
	
	public void setItem(OverlayItem i) {
		item = i;
		populate();
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		Log.v("CURR LOC", item.getPoint().toString());
		return item;
	}
	
	@Override
	public int size() {
		return 1;
	}
}