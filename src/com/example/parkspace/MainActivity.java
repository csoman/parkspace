package com.example.parkspace;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.example.parkspace.client.RESTClient;
import com.example.parkspace.client.SellerLocation;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.Toast;

public class MainActivity extends MapActivity {
	public static String userId = "user-" + System.currentTimeMillis();
	public static Context mainContext;
	
	private MapController myMapController;
	private List<Overlay> mapOverlays;
	
	private final int PinnedLocationIndex = 0;
	private final int ExistingSellerIndex = 1;
	private final int CurrentLocationIndex = 2;
	private final int ConfirmButtomIndex = 3;
	
	public static final int DefaultLat = 37428245;
	public static final int DefaultLon = -122174062;
	
	// SF Park:
    private final String SFParkURL = "http://api.sfpark.org/sfpark/rest/availabilityservice?radius=2.0&response=json&pricing=yes&version=1.0";

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	private void updateSingleItem(int index, GeoPoint point) {
		OverlayItem overlayitem = new OverlayItem(point, "Not filled in", "Not filled in");
		((SingleItemizedOverlay)mapOverlays.get(index)).setItem(overlayitem);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapView mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		myMapController = mapView.getController();
		
		mainContext = getApplicationContext();
		mapOverlays = mapView.getOverlays();
		
	    mapOverlays.add(new TouchMovingSingleItemizedOverlay(getResources().getDrawable(R.drawable.pin))); // Pinned location, index 2
		mapOverlays.add(new ExistingSellersItemizedOverlay(getResources().getDrawable(R.drawable.pin))); // Existing sellers, index 0
		mapOverlays.add(new SingleItemizedOverlay(getResources().getDrawable(R.drawable.curr))); // Current location, index 1
		mapOverlays.add(new SometimesVisibleButtonItemizedOverlay(getResources().getDrawable(R.drawable.confirm), getApplicationContext())); // Confirm button location, index 3
		// connect the confirm overlay to the pin overlay
		((TouchMovingSingleItemizedOverlay)mapOverlays.get(PinnedLocationIndex)).setTouchButton((SometimesVisibleButtonItemizedOverlay)mapOverlays.get(ConfirmButtomIndex));
		
		// Get my location
		/*LocationManager mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		LocationListener mlocListener = new MyLocationListener();
		mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, mlocListener);
		*/
		myMapController.animateTo(new GeoPoint(DefaultLat, DefaultLon));
		myMapController.setZoom(19);
		
		//Timer refreshTimer = new Timer();
		//refreshTimer.schedule(new RefreshTask(), 0, 500);
		LoadExistingSellersTask les = new LoadExistingSellersTask();
        les.execute("String");
        
        Log.e("ParkSpace4" , "User ID : " + MainActivity.userId);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	class RefreshTask extends TimerTask {
		public RefreshTask() {
			
		}
		
		public void run() {
			LoadExistingSellersTask les = new LoadExistingSellersTask();
	        les.execute("String");
		}
	}
	
	private class LoadExistingSellersTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... arg0) {
//			while (true) {
			List<SellerLocation> locations = RESTClient.getParkingLocations();

			ExistingSellersItemizedOverlay sellerOverlay = (ExistingSellersItemizedOverlay)(mapOverlays.get(ExistingSellerIndex));
			if (locations == null) {
				Log.e("ParkSpace", "LOC NULL");
				return null;
			}
			/*for (SellerLocation loc : locations) {
				Log.e("ParkSpace", "got location: " + loc.toString());
				int lat = (int)loc.getLatitude();
				int longt = (int)loc.getLongitude();
				GeoPoint gp = new GeoPoint(lat, longt);
				sellerOverlay.addItem(new OverlayItem(gp, "Not filled in", "Not filled in"));
				
				GeoPoint gp2 = new GeoPoint(DefaultLat, DefaultLon);
				sellerOverlay.addItem(new OverlayItem(gp2, "Not filled in", "Not filled in"));
				
			}*/
			Log.e("ParkSpace", "Retrieved size " + locations.size());
			
			sellerOverlay.clear();
			sellerOverlay.addItems(locations);
		
			try { Thread.sleep(500); } catch (Exception e) {}
//			}
			return null;
		}
		
	}
	
	private class LoadSFParkTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... arg0) {
			try {
				String data = Helpers.getStringContent(SFParkURL);
				JSONObject root = new JSONObject(data);
			} catch (Exception e) {
				Log.v("Net", "Couldn't reach SF Park. " + e.toString());
			}
			return null;
		}
		
	}
	
	private class MyLocationListener implements LocationListener {

		public void onLocationChanged(Location location) {
			location.getLatitude();
			location.getLongitude();
			String Text = "My current location is: Latitud = " + location.getLatitude() + "Longitud = " + location.getLongitude();
			//Toast.makeText( getApplicationContext(), Text, Toast.LENGTH_SHORT).show();
			GeoPoint myGeoPoint = new GeoPoint(
				    (int)(location.getLatitude()*1000000),
				    (int)(location.getLongitude()*1000000));
			updateSingleItem(CurrentLocationIndex, myGeoPoint);
			myMapController.animateTo(myGeoPoint);
		}

		public void onProviderDisabled(String provider) {
			//Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();			
		}

		public void onProviderEnabled(String provider) {
			//Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT ).show();
			
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
	}
}
